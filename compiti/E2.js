/*
Scrivere una funzione elimina(a,s) che, dati in input un array a di numeri e un numero s,
modifica a eliminando gli elementi in fondo ad a fino a che la somma degli elementi
eliminati non supera s. La funzione elimina(a,s) restituisce poi l'array a modificato.
*/

const prompt=require("prompt-sync")({sigint:true}); 


function elimina( a, s )
  {
    var sum = 0;
    for( i = a.length-1; i >= 0 && sum <= s; i-- )
      {
        sum += a[i];
      }
    
    a.length = i+1;
  }


const array = new Array();
let stop = false

do
  {
    let v = prompt( "insert element: " );
    if( v != "STOP!" )
      {
        v = Number(v);
        array.push( v );
      }
    else
      {
        stop = true;
      }
  }
while( !stop );

let s = Number( prompt( "insert: " ) );

elimina( array, s )
console.log( array );