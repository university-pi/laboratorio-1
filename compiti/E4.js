/*
Scrivere una funzione calcola(a) che, dato un array di punti sul piano cartesiano (ciascuno del tipo { x: valX, y: valY }), determina il centroide di tali punti, ovvero il punto le cui coordinate sono date dalla media aritmetica delle coordinate di tutti i punti nell'array. (Se a è vuoto, restituisce il punto con coordinate 0,0)
*/

const prompt=require("prompt-sync")({sigint:true}); 

function Point( x, y )
  {
    this.x = x;
    this.y = y;
  }

function calcola( a )
  {
    let xs = 0;
    let ys = 0;
    let els = 0;

    for ( let p of a )
      {
        xs += p.x;
        ys += p.y;
        els++;
      }
    let cx= ( els == 0) ? 0 : xs / els;
    let cy= ( els == 0) ? 0 : ys / els;
    return new Point( cx, cy );
  }

const array = new Array();
let it = 0;
while( true )
  {
    let x = 0;
    let y = 0;
    x = prompt( "Inserisci x: " );
    if( x == "!stop" )
      {
        break;
      }
    y = prompt( "Inserisci y: " );

    array.push( new Point( Number(x), Number(y) ) );
  }



console.log( calcola( array ) );