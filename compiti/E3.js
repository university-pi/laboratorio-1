/*
Si scriva una funzione azzera(a,p) che prende in input un array a di stringhe e una
funzione p. La funzione p implementa un predicato, prendendo in input una stringa e
restituendo true o false. L'invocazione di azzera(a,p) sostituisce ogni elemento in a per
cui p(a)==true con "". 
*/

const prompt=require("prompt-sync")({sigint:true}); 

let p = ( string ) =>
  {
    return (Boolean(string) == true)
  }

function azzera( array, predicato )
  {
    for( var i of array )
      {
        if( predicato( a[i] ) )
          {
            delete a[i];
          }
      }
  }

const array = new Array( );

do
  {
    let input = prompt( "insert " );
    if( input == "!stop" )
      continue;
    else
      array.push( input );
  }
while( input != "!stop" );

