/*
Scrivere una funzione sposta(p) che prende in input un oggetto p che rappresenta un punto del piano cartesiano, ovvero del tipo {x: valX, y: valY }. La funzione sposta(p) restituisce una funzione che prende due numeri n e m e restituisce il punto p concoordinate aggiornate sommando n sull'asse x e m sull'asse y.
*/


function Point(x, y)
  {
    this.x = x;
    this.y = y;
  }



function sposta( p )
  {
    x = p.x;
    y = p.y;
    function add( mx, my )
      {
        x += mx;
        y += my;

        return new Point( x, y );
      }
    return add;
  }

let punto = {x:12, y:22 };

let p = new Point( 0, 0 );

console.log( sposta(p)(5, 4) );