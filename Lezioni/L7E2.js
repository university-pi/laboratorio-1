/* Scrivere una funzione che, dati due punti (di cui si conoscono le coordinate x e y), restituisce il punto che è più lontano dall’origine (0,0) */

const prompt=require("prompt-sync")({sigint:true});


function Points( x, y )
  {
    this.x = x;
    this.y = y;
    this.dist = 0;
  }
Points.prototype.toString = function P2S()
  {
    return `(${this.x}, ${this.y})`;
  };

const O = new Points( 0, 0 );
function distance( point )
  {
    return ( (((point.x-O.x)**2)+((point.y-O.y)**2))**0.5 );
  }

const P1 = new Points();
const P2 = new Points();

P1.x = Number( prompt( "P1(x): " ) );
P1.y = Number( prompt( "P1(y): " ) );
P1.dist = distance( P1 );
P2.x = Number( prompt( "P2(x): " ) );
P2.y = Number( prompt( "P2(y): " ) );
P2.dist = distance( P2 );


console.log( (P1.dist > P2.dist) ? P1.toString() : P2.toString() )