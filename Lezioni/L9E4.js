/* Contare i bit settati a 1 in un numero a */

const prompt=require("prompt-sync")({sigint:true});

let num = Number( prompt( "insert num " ) );

num = num.toString(2).split('');

var sum = 0;
for ( let c of num )
  {
    sum += (c=='1') ? 1 : 0;
  }


console.log( num );
console.log( sum );