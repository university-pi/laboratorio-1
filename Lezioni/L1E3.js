/* Scrivere un programma che dato un numero (secondi) dalla tastiera, calcola e mostra a video il numero di ore, minuti, secondi */
const prompt=require("prompt-sync")({sigint:true}); 

let num = Number(prompt("Secondi?")); //get input
// let num = 7350;
const o2s = 3600; //ore to secondi
let restoOre = num%3600; //i secondi dell'input
let ore = (num-restoOre)/o2s;
let sec = restoOre%60;
let min = (restoOre - sec)/60;

console.log( ore + ":" + min + ":" + sec );