/* Scrivere una funzione che trova il massimo in un array una funzione che trova il minimo in un array e una funzione che sfrutta le funzioni già scritte per restituire massimo e minimo di un array (in un altro array) */

const array = [10,-22,12,-345,-1,3,-5];

function Max ( array )
  {
    var max = array[0];
    for ( el of array )
      {
        max = el > max ? el : max;
      }
    return max
  }

function min ( array )
  {
    var min = array[0];
    for ( el of array )
      {
        min = el < min ? el : min;
      }
    return min
  }


console.log( "max: " + Max( array ) + " - min: " + min( array ) );



