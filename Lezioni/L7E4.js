/* Modificare l’esempio precedente in modo che il programma stampi anche la quantità di volte che una parola è stata inserita. */

const prompt=require("prompt-sync")({sigint:true});


const ws = new Array();

do
  {
    var word = prompt( "insert: " );
    if( ws[word] != undefined )
      {
        ws[word]++;
      }
    else
      {
        ws[word] = 1;
      }
  }
while( word != "BASTA!" );

for( let word in ws )
  {
    if( word !== "BASTA!")
      console.log( word + ": " + ws[word] );
  }
