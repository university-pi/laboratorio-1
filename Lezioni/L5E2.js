/* Disegnare il grafico e stimare il minimo della funzione f(x) = (x+0.1)**2. Sperimentare con la precisione per ottenere il minimo */

// const prompt=require("prompt-sync")({sigint:true}); 

let f = ( x => (x+0.1)**2);
const min = [ undefined, undefined ];
const delta = 0.5;
for (x = -10; x <= 10; x += delta) 
  {
    if( min[0] == undefined && min[1] == undefined )
      {
        min[0] = [x]
        min[1] = [f(x)];
      }
    if( f(x) < min[1] )
      {
        min[0] = [x]
        min[1] = [f(x)];
      }
    
    colore("#00aa00");
    linea( x, f(x), x+1, f(x+1) );
  }

  colore( "#FF0000" );
  punto( min[0], min[1] );
  console.log( min[0] + " " + min[1] );