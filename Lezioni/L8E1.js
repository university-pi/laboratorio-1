/* [FusoOrario] Scrivere (e testare) una funzione Javascript che, dato un oggetto che rappresenta un orario in termini di hh, mm e ss, modifica hh secondo applicando fuso orario (positivo o negativo) fornito in input */

const prompt=require("prompt-sync")({sigint:true});

let date = new Date();

function Hour( hh, mm, ss )
  { 
    this.hh = hh;  
    this.mm = mm;  
    this.ss = ss;
  }

Hour.prototype.toString = function H2S() {return `${this.hh}:${this.mm}:${this.ss}`;};


let UTC = Number( prompt( "Time Zone: " ) );

let hour = new Hour( ((date.getHours()+UTC)%24), date.getMinutes(), date.getSeconds() );

console.log( hour.toString() );