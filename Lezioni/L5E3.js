/* Disegnare i grafici sull’intervallo [0,50] per le funzioni f(x)=log(x), f(x) = 3x+2, f(x) = x2, f(x)=2x. Quale funzione cresce più velocemente?
Per il logaritmo e l’esponenziale usare le funzioni Math.log e Math.pow */


let log = ( x => Math.log(x) );
let f = ( x => (3*x)+2 );
let f1 = ( x => x**2 );
let exp = ( x => Math.pow(2, x) );
const max = [-1, -1, -1, -1];
const delta = 0.5;
for (x = 0; x <= 50; x += delta) 
  {
    max[0] = log(x);
    max[1] = f(x);
    max[2] = f1(x);
    max[3] = exp(x);
    colore("#00aa00");
    linea( x, log(x), x+delta, log(x+delta) );
    colore("#FFaa00");
    linea( x, f(x), x+delta, f(x+delta) );
    colore("#FFaaFF");
    linea( x, f1(x), x+delta, f1(x+delta) );
    colore("#00aaFF");
    linea( x, exp(x), x+delta, exp(x+delta) );
  }

let Max = 0;
let tmp = 0;
for( i = 0; i< max.length-1; i++ )
  {
    if( max[i] > Max )
      {
        tmp = max[i];
        Max = i;
      }
  }

let string = "La funzione più veloce è";
switch( Max )
  {
    case 0:
      string += " il logaritmo";
      break;
    case 1:
      string += " la f(x)";
      break;
    case 2:
      string += " la f1(x)";
      break;
    case 3:
      string += " l'esponenziale";
      break;
  }

console.log( string );