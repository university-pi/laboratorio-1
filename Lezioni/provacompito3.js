/* Scrivere funzione prettyPrint che, dato in input un oggetto o, stampa l’oggetto in come insieme di coppie chiave -> valore. (Si assuma che i valori associati a ciascuna chiave non siano a loro volta altri oggetti) */

const prompt=require("prompt-sync")({sigint:true});


let prettyPrint = (OBJ) => {
  for( let key in OBJ )
    {
      console.log( key + "->" + OBJ[key] );
    }
};

const obj1 = {"ciao": "mamma"};
const obj2 = {voto: -1, materia: "laboratorio 1"};
const obj3 = {"pippo": 0, 1: "paperino", topolino: "minnie"};
const obj4 = {};

prettyPrint( obj1 );
prettyPrint( obj2 );
prettyPrint( obj3 );
prettyPrint( obj4 );