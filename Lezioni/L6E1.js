/* Scrivere una funzione che somma di tutti gli elementi presenti in un array */

const array = [10,22,12,345,1,3,5];

let sum = 0;
for ( el of array )
  {
    sum += el;
  }
console.log( sum );


/* E se la invochiamo su ["super","cali","fragi","listico"]? */

const array1 = ["super", "cali", "fragi", "listico"];

let sum1 = 0;
for ( el of array1 )
  {
    sum1 += el;
  }
console.log( sum1 );