/* Funzione che stampa la rappresentazione in complemento a 2 per un numero intero */

const prompt=require("prompt-sync")({sigint:true});


function complement2( val )
  {
    return (~val)+1;
  }


let val = Number( prompt( "get num: " ) );

console.log( complement2( val ) );
