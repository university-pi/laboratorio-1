/* [Accumulatore] Si scriva un programma che legge numeri fino a che la loro somma non supera 101. Il programma deve poi stampare la somma ottenuta */

const prompt=require("prompt-sync")({sigint:true}); 

var sum = 0;

while ( sum <= 101 )
  {
    sum += Number(prompt("Insert: "));
  }

console.log( sum );