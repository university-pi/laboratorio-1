/* Scrivere un programma che legge da tastiera una lista parole fino a che l’utente non scrive “BASTA!”. A quel punto il programma deve stampare la lista delle parole inserite. */

const prompt=require("prompt-sync")({sigint:true});


const words = new Array();

do
  {
    words.push( prompt( "insert: " ) );
  }
while( words[words.length-1] != "BASTA!" )

for (let word of words )
  {
    if( word !== "BASTA!" )
      console.log( word );
  }