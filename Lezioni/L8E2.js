/* [Strcat] Scrivere (e testare) una funzione che, dato un array in input, restituisce la concatenazione di tutti i suoi elementi (interpretandoli come stringhe) */

const prompt=require("prompt-sync")({sigint:true});

function strcat( array )
  {
    let str = "";
    for( let el of array )
      {
        str += `${el}`;
      }
    
    return str
  }


const array = new Array();

for( i=0; i<5; i++ )
  {
    array.push( prompt("insert: ") );
  }

console.log( strcat( array ) );