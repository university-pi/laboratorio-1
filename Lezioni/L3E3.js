/* [MultiPrimo] Si modifichi il programma di sopra in modo che legga 10 numeri n e calcoli per ciascuno se si tratta di un numero primo */

const prompt=require("prompt-sync")({sigint:true}); 

var primes = [];
for( var i = 0; i < 10; i++ )
  {
    primes.push( Number(prompt("Insert: ")));
  }


for( var i = 0; i < 10; i++ )
  {
    var n = primes.pop();
    var j = 4, p=true;
    
    if( n <= 1 || n % 2 == 0 || n % 3 == 0 )
      p = false;
    if( n == 2 || n == 3 )
      p = true;
    while( j <= Math.floor(n**0.5) && p )
      {

        if( ((n % j) == 0) && p )
          {
            p = false;
          }
        j++;
      }
    // console.log(p);
    console.log( n + " is" + ((p==true) ? " prime" : " not prime") );
  }