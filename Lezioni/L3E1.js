/* [Sommatoria] Si scriva un programma che, dato un intero positivo n, calcola e stampa la somma dei numeri dispari da 1 a n (con n compreso) */

const prompt=require("prompt-sync")({sigint:true}); 

var n = Number(prompt( "Number: "));

if( n > 0 )
  {
    var sum = 0;
    for( i = 0; i <= n; i++ )
      {
        sum += ((i%2)==0) ? 0 : i; 
      }
    console.log( sum );
  }