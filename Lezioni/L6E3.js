/* Scrivere una funzione che scambia gli elementi nelle posizioni i e j di un array */

function swap( array, i, j )
  {
    var tmp = array[i];
    array[i] = array[j];
    array[j] = tmp;
  }


const array = [10,22,12,345,1,3,5];
swap(array, 0, array.length-1 );
console.log( array );