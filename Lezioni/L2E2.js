/* [Max] Scrivere un programma che legga da tastiera tre numeri e mostri a video il loro massimo */

const prompt=require("prompt-sync")({sigint:true}); 


var max = 0;
for( var i = 0; i < 3; i++ )
  {
    var n = Number(prompt( `Numero ${i+1} ` ));
    if( n > max )
      max = n;
  }

console.log( max );