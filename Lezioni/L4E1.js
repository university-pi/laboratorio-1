/* (Media) Si scriva un programma che legga da tastiera 10 interi e stampi la media aritmetica di tutti i valori diversi da zero e di segno uguale all’ultimo valore della sequenza. */

const { exit } = require("process");

const prompt=require("prompt-sync")({sigint:true});

const seq = new Array();

for( var i = 0; i < 10; i++ )
  {
    seq.push( Number(prompt("insert: ")));
  }

let len = seq.length-1;
let last = seq[len];
let positive = true;
if( last != 0 )
  {
    if( last > 0 )
      {
        positive = true;
      }
    else
      {
        positive = false;
      }
  }
else
  {
    exit(1);
  }

let sum = 0;
let count = 0;
for( var i = 0; i < 10; i++ )
  {
    if( seq[i] > 0 && positive )
      {
        sum += seq[i];
        count++;
      }
    if( seq[i] < 0 && !positive )
      {
        sum += seq[i];
        count++;
      }
  }

console.log( sum/count );