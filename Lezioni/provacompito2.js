/* Scrivere funzione strStar che, dato in input un array a, modifica a in modo contenga tutte stringhe di lunghezza 10 (se la stringa/cifra è più corta, aggiunge tanti “*” quanti necessari a raggiungere la lunghezza 10).(Si assuma a come contenente stringhe e numeri) */


const prompt=require("prompt-sync")({sigint:true});


let strStar = function strstar( str )
  {
    if( str.length < 10 )
      {
        while( str.length < 10 )
          {
            str = str.concat( "*" );
          }
      }
    if( str.length > 10 )
      {
        str = str.substring(0,10);
      }
      
    return str;
  }


let words = new Array();
let starWord = new Array();

do
  {
    words.push( prompt( "insert " ) );
  }
while( words[words.length-1] != "STOP!" )

words.length--;

for( let word of words )
  {
    starWord.push( strStar( word ) );
  }

for( let word of starWord )
  {
    console.log( word );
  }