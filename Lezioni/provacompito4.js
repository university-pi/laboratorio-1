/* Scrivere una funzione walkingDistance che, dato un array di coordinate (oggetti {x: valore, y: valore}), calcola la distanza totale percorsa. (Usare Math.sqrt(n) per calcolare la radice quadrata di n) */

const prompt=require("prompt-sync")({sigint:true});


let walkingDistance = ( array ) => {
  let x0 = 0;
  let y0 = 0;
  let dist = 0;
  for( cs of array )
    {
      let x1 = cs.x;
      let y1 = cs.y;
      dist += Math.sqrt( ((x1-x0)**2) + ((y1-y0)**2) );
      x0=x1;
      y0=y1;
    }
  return dist;
};


let obj1 = [{x:0,y:1},{x:0,y:2},{x:0,y:3},{x:0,y:4}];
let obj2 = [{x:1,y:0},{x:2,y:0},{x:3,y:0},{x:4,y:0}];
let obj3 = [{x:0,y:1},{x:1,y:1},{x:2,y:1},{x:2,y:2}];
let obj4 = [];


console.log( walkingDistance(obj1) );
console.log( walkingDistance(obj2) );
console.log( walkingDistance(obj3) );
console.log( walkingDistance(obj4) );