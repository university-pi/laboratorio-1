/* Scrivere funzione abs che, dato in input un array a, restituisce una copia di a contenente il valore assoluto di tutti gli elementi in a. (Si assuma a come contenente numeri) */

const prompt=require("prompt-sync")({sigint:true});

let abs = function ABS( val )
  {
    return (val < 0) ? val*(-1) : val;
  }

let a = new Array();

do
  {
    a.push( Number(prompt( "insert: " ) ) );
  }
while( a[a.length-1] != -1995 )

a.length -= 1;


let acopy = new Array();

for ( let val of a )
  {
    acopy.push( abs( val ) );
  }

for (let valcopy of acopy )
  {
    console.log( valcopy );
  }