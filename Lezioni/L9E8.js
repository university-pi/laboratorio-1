/* Ricerca binaria - funzione che cerca un elemento in un array ordinato e restituisce la sua posizione */

const prompt=require("prompt-sync")({sigint:true});


const binSearch = ( array, value ) => {
  let trovato = false;
  let l = 0;
  let r = array.length-1;
  while( !trovato || l <= r )
    {
      let c = Math.floor( ( l + r ) / 2 );
      if( array[c] == value )
        {
          return c;
        }
      if( array[c] < value )
        {
          l = c + 1;
        }
      if( array[c] > value )
        {
          r = c - 1;
        }
    }
  return null;
};



let a = [0,1,2,3,4,5,6,7,8,9];
console.log( binSearch( a, 5 ) );