/* [Pari] Scrivere un programma che legga da tastiera un numero e stabilisca se quel numero è pari o dispari, stampando in uscita rispettivamente 1 o 0 */

const prompt=require("prompt-sync")({sigint:true}); 

var IN = Number(prompt("Numero: "));

console.log( Number(!(IN % 2)) );