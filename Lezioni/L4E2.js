/* (Minimo) Si scriva un programma che legga da tastiera 10 interi e stampi il valore minimo. Calcolo del minimo/massimo tra un set di valori & Iterando e mantenendo il minimo/massimo corrente */

const prompt=require("prompt-sync")({sigint:true}); 

var min1 = undefined;
const array = new Array();

for ( var i = 0; i < 10; i++ )
  {
    if( min1 === undefined )
      {
        min1 = Number(prompt("insert: "));
        array.push( min1 );
      }
    else
      {
        var tmp = Number(prompt("insert: "));
        min1 = (tmp < min1) ? tmp : min1;
        array.push(tmp);
      }
  }

var min2 = array[0];
for( var i = 1; i < 10; i++ )
  {
    min2 = (array[i] < min2) ? array[i] : min2;
  }

console.log( min1 + " - " + min2 );