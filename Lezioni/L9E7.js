/* Ricerca lineare - funzione che cerca un elemento in un array e restituisce la sua posizione */

const prompt=require("prompt-sync")({sigint:true});


const search = ( array, value ) => {
  let i = 0;
  while( array[i] != value )
    {
      i++;
    }
  return i;
};

const a = [0,1,2,3,4,5,6,7,8,9];

console.log( search( a, 2 ) );