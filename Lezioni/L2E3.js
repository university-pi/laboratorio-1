/* [Calcolatore] Scrivere un programma che legga da tastiera due numeri ed un operatore (tra +, -, * e /) e mostri a video il risultato dell’operazione tra i due numeri */

const prompt=require("prompt-sync")({sigint:true}); 


const nums = [];
for( var i = 0; i < 2; i++ )
  {
    var n = Number(prompt( `Numero ${i+1} ` ));
    nums.push(n);
  }


var opt = prompt( "Operazione: " );

switch (opt)
  {
    case '+':
      var b = nums.pop();
      var a = nums.pop();
      console.log( a+b );
      break;
    case '-':
      var b = nums.pop();
      var a = nums.pop();
      console.log( a-b );
      break;
    case '*':
      var b = nums.pop();
      var a = nums.pop();
      console.log( a*b );
      break;
    case '/':
      var b = nums.pop();
      var a = nums.pop();
      if( b != 0 )
        console.log( a/b );
      else
        console.log( "you cannot divide by 0");
      break;
    default:
        console.log( "WTF bro?" );
  }
