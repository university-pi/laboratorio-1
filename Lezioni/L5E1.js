/* Disegnare il grafico sull’intervallo [-50,50] per la funzione f(x) = x/2-1 */

let f = (x => (x/2)-1);

for ( var i = -50; i <= 50; i++ )
  {
    colore( "#0000FF")
    linea(i, f(i), i+1, f(i+1) );
    // console.log( i, f(i));
  }