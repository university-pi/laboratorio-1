/* Scrivere una funzione che prende in input un array e lo restituisce “invertito” */

const prompt=require("prompt-sync")({sigint:true});


function swapArray( array )
  {
    var i = 0, j = array.length-1;
    while( i < j )
      {
        var tmp = array[i];
        array[i] = array[j];
        array[j] = tmp;
        i++;
        j--;
      }
  }

var len = Number(prompt( "Array Length: "));
const array = [];
for( i = 0; i < len; i++ )
  {
    array[i] = Number(prompt( `Element ${i} `));
  }

console.log( "\n \t" + array + "\n" );
swapArray( array );
console.log( "\n \t" + array + "\n" );
swapArray( array );
console.log( "\n \t" + array + "\n" );