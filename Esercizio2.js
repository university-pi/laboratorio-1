/* Si scriva un programma che, dato un intero positivo n, calcola e stampa la somma dei numeri dispari da 1 a n (con n compreso) */

const prompt=require("prompt-sync")({sigint:true}); 

let n = Number(prompt("Numero? "));
let sum = 0;
for( i = 1; i <= n; i++ )
  if( i%2 != 0 )
    sum += i;

console.log( sum )