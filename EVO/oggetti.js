/*** ES1 ***/
// Una biblioteca ha una collezione di libri con titolo, autore e numero di copie disponibili. Un libro è rappresentato mediante un oggetto avente 3 proprietà: titolo, autore, copie. La biblioteca viene rappresentata da un array di libri. Si scriva una funzione prestito(biblioteca, s), con biblioteca un array di libri, e s una stringa che rappresenta il titolo di un libro. Se in biblioteca è presente un libro avente titolo s, quel libro viene dato in prestito: il numero di copie di quel libro in biblioteca viene decrementato, e la funzione restituisce true. Se il libro non è disponibile (o non esiste in biblioteca, o non ci sono copie disponibili), la funzione restituisce false, e biblioteca non viene modificato.
function prestito( biblioteca, s )
  {
    for( let book of biblioteca )
      {
        if( book.titolo == s && book.copie > 0 )
          {
            book.copie -= 1;
            return true;
          }
      }
    return false;
  }



/*** ES2 ***/
// Si scriva una funzione norma( v ) che, dato un oggetto v con chiavi x e y con valore float, calcoli la norma euclidea del vettore e la aggiunga a v con chiave norma.
function norma( v )
  {
    v['norma'] = Math.sqrt( (v.x)**2 + (v.y)**2 );
    return v;
  }



/*** ES3 ***/
// Dato un array di veicoli rappresentati come oggetti con chiavi tipo, cilindrata e peso, si scriva una funzione sort_vehicles che ordini l’array:
// per tipo (alfabeticamente),
// a parità di tipo, per cilindrata (crescente),
// a parita di tipo e cilindrata, per peso (crescente).
function sort_vehicles( v )
  {
    v.sort( ( a, b ) => 
      {
        const va = a.tipo.toUpperCase();
        const vb = b.tipo.toUpperCase();
        if( va < vb )
          return -1;
        if( va > vb )
          return 1;
        
        return 0;
      }
    );

    v.sort( (a, b) => 
      {
        if( a.tipo == b.tipo )
          {
            if( a.cilindrata < b.cilindrata )
              return -1;
            if( a.cilindrata > b.cilindrata );
              return 1;
            return 0;
          }
      }
    );
    v.sort( (a, b) => 
      {
        if( a.tipo == b.tipo && a.cilindrata == b.cilindrata )
          {
            if( a.peso < b.peso )
              return -1;
            if( a.peso > b.peso );
              return 1;
            return 0;
          }
      }
    );
    
    return v;
  }



/*** ES4 ***/
// Si scriva una funzione calcT(a) che, dato come argomento un array a, in cui il primo elemento è un operatore aritmetico (+, -, *, /) rappresentato come carattere, restituisca il risultato ottenuto applicando l’operatore fra tutti i rimanente elementi dell’array, con la cautela che se uno degli altri elementi è esso stesso un array, viene prima valutato il suo valore secondo la stessa regola.
// Suggerimenti: splendida occasione per un assegnamento destrutturante.
function ops( op, arg1, arg2 )
  {
    switch( op )
          {
            case '+':
              return arg1 + arg2;
            case '-':
              return arg1 - arg2;
            case '*':
              return arg1 * arg2;
            case '/':
              return arg1 / arg2;
            default: return;
         }
  }

function calcT( a )
  {
    const [op, arg1, arg2] = a;
    if( typeof( arg1 ) === 'number' && typeof( arg2 ) === 'number' )
      {
        return ops( op, arg1, arg2 );
      }
    if( Array.isArray( arg1 ) && typeof( arg2 ) === 'number' )
      return ops( op, calcT( arg1 ), arg2 );
    if( Array.isArray( arg2 ) && typeof( arg1 ) === 'number' )
      return ops( op, arg1, calcT( arg2 ) );
    if( Array.isArray( arg1 ) && Array.isArray( arg2 ) )
      return ops( op, calcT( arg1 ), calcT( arg2 ) );
  }
console.log( calcT(["+",4,["*",3,2]]) );
console.log( calcT(["*",4,5,2]) );
console.log( calcT(["/",64,["*",["+",2,2],4]]) );



/*** ES5 ***/
// Si scriva una funzione omerge(o1,o2) che, dati due oggetti o1 e o2, restituisca un nuovo oggetto contenente tutte le proprietà di o1 e tutte quelle di o2, escluse quelle che compaiono sia in o1 che in o2.
function omerge( o1, o2 )
  {
    let newobj = {};
    for( let prop in o1 )
      {
        if( !o2.hasOwnProperty(prop) )
          newobj[`${prop}`] = o1[prop];
      }
    for( let prop in o2 )
      {
        if( !o1.hasOwnProperty(prop) )
          newobj[`${prop}`] = o2[prop];
      }
    return newobj;
  }



/*** ES6 ***/
// Sia dato l'oggetto veicolo con chiavi tipo e altezza con valori, rispettivamente, stringa e float (e.g., {'tipo': tir, 'altezza': 3.2}. Si scriva una funzione tunnel(convoy, max_height) che, dato un array di veicoli convoy e un'altezza massima max_height, restituisca un array contenente tutti i veicoli con altezza minore di max_height. In caso di array vuoto o indefinito, si restituisca un array vuoto.
// Bonus: se l'array convoy è definito, si modifichi in-place.
function tunnel( convoy, max_height )
  {
    if( convoy === undefined || convoy === null )
          return [];
    for( let v of convoy )
      {
        if( v.altezza >= max_height )
          {
            convoy.splice( convoy.indexOf(v), 1 );
          }
      }

    return convoy;
  }



/*** ES7 ***/
// Si scriva una funzione modifica(ao), con ao un array di oggetti. modifica(ao) deve restituire una funzione che, preso come argomento un array di stringhe ak, “modifichi” gli oggetti contenuti in ao in modo da eliminare in ciascun oggetto tutte le proprietà le cui chiavi non compaiono nell’elenco di ak. La funzione deve restituire il numero complessivo di proprietà eliminate.
function modifica( ao )
  {
    function rm( sup )
      {
        let counter = 0;
        for( let o of ao )
          {
            for( let k in o )
              {
                if( !sup.includes( k ) )
                  {
                    delete o[k];
                    counter++;
                  }
              }
            }
        return counter;
      }    
    return rm;
  }
