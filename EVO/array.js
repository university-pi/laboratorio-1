/*** ES1 ***/
// Si definisca una funzione replace(arr, target, replacement) che dato un array arr ritorni un array dove tutte le instanze di target sono sostituite dall'elemento replacement. La funzione deve ritornare un nuovo array e non modificare quello passato come argomento.
function replace( arr, target, replacement )
  {
    const newarr = new Array();
    for( let el of arr )
      {
        if( el === target )
          {
            newarr.push( replacement );
          }
        else
          {
            newarr.push( el );
          }
      }
    return newarr;
  }



/*** ES2 ***/
// Scrivere una funzione JS moveToEnd(a, k), dove a è un array che rappresenta una lista di numeri, e k un intero. La funzione cerca la prima occorrenza di k nella lista. Se k si trova nella lista, la funzione restituisce la sua posizione nella lista (contando da 0) e porta l’elemento che contiene k in fondo alla lista, altrimenti restituisce -1.
// Nota: Si assuma che la testa della lista sia l'elemento 0 di a, mentre la coda l'ultimo.
function moveToEnd( a, k )
  {
    let pos = -1;
    let i = 0;
    while( a[i] !== k && i < a.length )
      {
        i++
      }
    if( a[i] === k )
      {
        pos = i;
        let t = a[i];
        a.splice( i, 1 );
        a.push( t );
      }

    return pos;
  }


/*** ES3 ***/
// Si scriva una funzione map_senior che, dato un array contenente oggetti persona con chiavi nome ed eta, restituisca lo stesso array dove a tutti gli oggetti viene aggiunta una chiave maggiorenne con valore booleano che dica se l'età della persona è >= 18.
function map_senior( db )
  {
    db.forEach(
      element => {
       element['maggiorenne'] = (element.eta < 18) ? false : true ;
      }
    );
    return db;
  }



/*** ES4 ***/
// Si scriva una funzione raggruppa_nascita(persone) che dato un array di oggetti con chiavi nome, annonascita, e luogonascita, restituisca un oggetto che ha come chiavi i diversi anni di nascita e come valori degli array che raggruppino i rispettivi oggetti.
// NOTA: L'ordine degli oggetti negli array deve preservare l'ordine dell'array persone.
function raggruppa_nascita( persone )
  {
    let obj = {};
    for( let p of persone )
      {
        if( !obj.hasOwnProperty( `${p.annonascita}`) )
          {
            obj[ `${p.annonascita}`] = new Array();
          }
        obj[`${p.annonascita}`].push( p );
      }
    return obj;
  }



/*** ES5 ***/
// Definire una funzione filter_replace(f,g), che prende in input una funzione f ed un predicato g, ovvero una funzione g che restituisce un valore booleano quando viene invocata. filter_replace(f,g) deve restituire una funzione che, preso in input un array A, prima filtra A creando un nuovo array B che contiene tutti i valori di A per cui g è vero, poi applica la funzione f su ogni elemento di B, memorizza il risultato in un nuovo array C e restituisce C.
// ATTENZIONE: L'array A non dev'essere modificato; l'array C deve preservare l'ordine degli elementi in A; non si possono usare le funzioni map e filter di libreria.
function filter_replace( f, g )
  {
    const b = new Array( );
    function apply( a )
      {
        a.forEach( 
            element => 
              {
                if( g( element ) )
                  b.push( element );
              }
            );
          const c = new Array( );
          b.forEach( 
          element => 
              {
                c.push( f(element) );
              }
            );
          return c;
      }
    return apply;  
  }
