/*** ES1 ***/
// Si scriva una funzione firma(s) che, data una stringa s, restituisca un intero positivo k calcolato come segue: si immagini di sostituire ogni vocale (maiuscola o minuscola) o spazio in s con 1, e qualunque altro carattere con 0. Si consideri poi la stringa risultante come un numero binario, e sia k il suo valore.
function firma( s )
  {
    let bits = [];
    s = s.toLowerCase();
    for( let c of s )
      {
        switch( c )
          {
            case 'a': bits.push( 1 ); break;
            case 'e': bits.push( 1 ); break;
            case 'i': bits.push( 1 ); break;
            case 'o': bits.push( 1 ); break;
            case 'u': bits.push( 1 ); break;
            case ' ': bits.push( 1 ); break;
            default: bits.push( 0 ); break;
          }
      }
    let n = 0;
    for( let i = 0; i < bits.length; i++ )
      {
        n += ( bits[i] * Math.pow(2, bits.length - i - 1) );
      }

    return n;
  }



/*** ES2 ***/
// Si scriva una funzione ordinaLen(a) che, dato un array di stringhe a, restituisca un array contenente le stesse stringhe, ordinate secondo la loro lunghezza (dalla più breve alla più lunga); a parità di lunghezza, andranno ordinate secondo l’ordine alfabetico.
function ordinaLen( a )
  {
    a.sort( (a,b) => {
      if( a.length < b.length || a < b )
        return -1;
      if( a.length > b.length || a > b )
        return 1;
      return 0;
    } );

    return a;
  }



/*** ES3 ***/
// La differenza simmetrica fra due insiemi A e B è l’insieme C che contiene gli elementi presenti solo in uno dei due (o in altre parole che non sono presenti nell’insieme unione di A con B).  Si implementi una funzione simmdiff(A,B) che, dati due insiemi A e B, restituisca l’insieme risultato della differenza simmetrica tra A e B, nel quale il valore massimo viene raddoppiato.
function simmdiff( a, b )
  { 
    let max = -Infinity;
    let c = {};
    for( let e1 in a )
      {
        if( !b.hasOwnProperty( e1 ) )
          {
            c[e1] = 1;
            max = ( Number(e1) > Number(max) ) ? e1 : max;
          }
      }
    for( let e2 in b )
      {
        if( !a.hasOwnProperty( e2 ) )
          {
            c[e2] = 1;
            max = ( Number( e2 ) > Number( max ) ) ? e2 : max;
          }
      }

    delete c[max];
    mmax = max * 2;
    c[mmax] = 1;
    return c
  }
a = {0:1, 4:1, 5:1, 9:1, 10:1, 544:1}; b = {0:1, 9:1, 22:1, 544:1};
console.log( simmdiff(a,b) );



/*** ES4 ***/
//Si scriva in JS una classe tabellaHash che implementa una tabella hash per memorizzare elementi numerici. La tabella hash è realizzata mediante un array di liste (chaining), dove le singole liste sono realizzate mediante array. Si devono implementare:
// - costruttore, che prende due argomenti, che rappresentano la dimensione della tabella (il primo, numero > 0) e la funzione hash utilizzata per inserire gli elementi nella tabella (il secondo). La funzione f è una funzione che prende come argomento un numero e restituisce un numero, e rappresenta la funzione hash utilizzata dalla tabella stessa.
// - metodo add(), che preso un numero n come argomento, lo inserisce nella tabella in posizione f(n), gestendo gli eventuali conflitti mediante le liste. Il metodo restituisce il numero di elementi totali memorizzati in tabella dopo l'inserimento.
// - metodo remove(), che preso un numero come argomento, lo elimina dalla tabella, se esiste. Il metodo restituisce il numero di elementi totali memorizzati in tabella dopo la rimozione.
// - metodo search(), che preso un numero come argomento, cerca il numero in tabella. In particolare, se l'elemento esiste in tabella, restituisce il primo indice a cui occorre l'elemento nella sua lista di appartenenza; altrimenti, -1.
// - metodo getTab() che non prende argomenti, e restituisce l'array di liste che rappresenta la tabella.
// - metodo addAll(), che preso un array di numeri come argomento, li inserisce tutti nella tabella, uno per volta in ordine, nelle rispettive posizioni f(.), gestendo gli eventuali conflitti mediante le liste. Il metodo restituisce il numero di elementi totali memorizzati in tabella dopo l'inserimento.
class tabellaHash
  {
    static foo( a, b )
      {
        return a+b;
      }
    constructor( size, hash )
      {
        this.size = size;
        this.ownHash = hash;
        this.tab = new Array( this.size );
        this.el = 0;
      }
    add( n )
      {
        let h = this.ownHash( n );
        if( this.tab[h] == null )
          {
            this.tab[h] = new Array();
          }
        this.tab[h].push( n );
        this.el += 1;

        return this.el;
      }
    remove( n )
      {
        let h = this.ownHash( n );
        let pos = this.search( n );
        if( pos != -1 )
          {
            this.tab[h].splice( pos, 1 );
            this.el -= 1;
          }

        return this.el;
      }
    search( n )
      {
        let h = this.ownHash( n );
        let pos = -1;
        if( this.tab[h] != null )
          { 
            for( let el of this.tab[h] )
              {
                pos = ( el === n ) ? this.tab[h].indexOf( el ) : pos;
              }
          }

        return pos;
      }
    getTab( )
      {
        return this.tab;
      }
    addAll( array )
      {
        for( el of array )
          {
            this.add( el );
          }
      }
  }
let th = new tabellaHash(11, (n) => n%11);

th.add(0);
th.add(11);
th.add(5);
th.add(2);
th.search(11);
th.remove(111);
th.getTab();
th.remove(0);
th.search(11);
th.getTab();
tabellaHash.foo(12,12);
