/*** E1 ***/
//  Si scriva una funzione contaCoppia(T,val1,val2) che dati un albero binario T (i cui nodi sono implementati come visto a lezione come oggetti con chiavi val, sx e dx) e due interi val1 e val2, restituisca il numero di coppie di nodi fratelli tali che il fratello destro abbia val maggiore o uguale a val1 e quello sinistro abbia val maggiore o uguale a val2. Due nodi sono fratelli se sono figli dello stesso nodo padre. Si faccia attenzione a conteggiare le coppie che soddisfano la condizione, non il numero dei nodi (che sarebbe ovviamente il doppio rispetto al numero di coppie).
function contaCoppia( T, val1, val2 ) 
  {
    if( T == null )
      {
        return 0;
      }
    if( T.sx != null && T.dx != null )
      {
        if( T.sx.val >= val1 && T.sx.val <= val2 )
          {
            1;
          }
      }
    return count + contaCoppia( T.sx, val1, val2 ) + contaCoppia( T.dx, val1, val2 );
  }



/*** E2 ***/
//  Si scriva una funzione JS maxDepth(T), dove T è un albero binario come descritto a lezione (oggetti con chiavi val e sx e dx). La funzione deve restituire la massima profondità dell'albero T: la lunghezza del cammino più lungo tra tutti quelli dalla radice alle sue foglie (la radice ha profondità zero).
function maxDepth(T) 
  {
    let psx = 0;
    let pdx = 0;
    if( T == null )
      return 0;
    if( T.sx != null)
      {
        psx = 1+maxDepth(T.sx);
      }
    if( T.dx != null)
      {
        pdx = 1+maxDepth(T.dx);
      }
    return Math.max(psx, pdx);
  }



/*** E3 ***/
// Si scriva una funzione verifica(T) che, dato un albero binario non vuoto T come definito a lezione (oggetti con chiavi val e sx e dx), restituisce true se il valore memorizzato in ogni nodo è minore o uguale di quello memorizzato nei suoi due figli, e false altrimenti. La proprietà è vera per una foglia.
function verifica(T) 
  {
    if( T == null )
      return true;
    if( T.sx == null && T.dx == null )
      {
        return true;
      }
    if( T.sx != null && T.dx != null )
      {
        if( T.sx.val < T.val || T.dx.val < T.val )
          return false;
      }
    return verifica(T.sx) && verifica(T.dx);
  }



/*** E4 ***/
// Si scriva una funzione JavaScript rpath(T,v) che, ricevuto un albero k-ario T, restituisca un array dei valori dei nodi dell'albero situati lungo il percorso da un nodo di T con proprietà val avente lo stesso valore e lo stesso tipo di v, alla radice, con sia il nodo che la radice inclusi. È garantito che i nodi dell'albero abbiano valori di val tutti distinti. Se non esiste in T un nodo con val uguale a v, la funzione restituisce undefined.
function rpath(T,v) 
  {
    if( T == null )
      {
        return undefined;
      }
    if( T.val === v )
      {
        return [T.val];
      }
    if( T.figli == null)
      {
        return undefined;
      }
    for( var i of T.figli )
      {
        var r = rpath( i, v );
        if( r != undefined )
          {
            return r.concat( T.val );
          }
      }
  }



/*** E5 ***/
// Si scriva una funzione isHeap(t) che, dato un albero k-ario t costruito come visto a lezione con nodi {val:v, figli: [t1, …, tn]}, restituisca true se l’albero soddisfa la proprietà di heap, ovvero il valore del padre è sempre maggiore del valore di ciascuno dei figli.
function isHeap( t ) 
  {
    if( t == null )
      return true;
    if( t.figli == null )
      return true;
    for( var i of t.figli )
      {
        if(  t.val <= i.val )
          {
            return false;
          }
      }
    for( var i of t.figli )
      {
        if(!isHeap( i ) )
          {
            return false;
          }
      }
  }



/*** E6 ***/
// Scrivere una funzione checkBST(tree) che, dato un albero tree, ritorna true se è un albero binario di ricerca, false altrimenti. Si assuma che in caso di valore uguale, un elemento sia inserito nel sottoalbero di destra. Come visto a lezione, gli alberi sono codificati tramite un oggetto con proprietà: val per il valore, sx per il sottoalbero di sinistra, e dx per il sottoalbero di destra.
// Un albero binario di ricerca (Binary Search Tree) ha le seguenti proprietà:
// -Il sottoalbero sinistro di un nodo x contiene soltanto i nodi con chiavi minori della chiave del nodo x;
// -Il sottoalbero destro di un nodo x contiene soltanto i nodi con chiavi maggiori della chiave del nodo x;
// -Il sottoalbero destro e il sottoalbero sinistro devono essere entrambi due alberi binari di ricerca.
function maxNode(tree) {
  if (tree === null){
      return - Infinity;
  }

  return Math.max(tree.val, Math.max(
      maxNode(tree.sx),
      maxNode(tree.dx)))
}

function minNode(tree) {
  if (tree === null){
      return Infinity;
  }

  return Math.min(tree.val, Math.min(
      minNode(tree.sx),
      minNode(tree.dx)))
}

function checkBST( tree ) 
  {
    if( tree == null )
      {
        return true;
      }
    if( tree.sx == null && tree.dx == null )
      {
        return true;
      }
    var mindx = minNode( tree.dx );
    var maxsx = maxNode( tree.sx );

    if( ( mindx != null && tree.val > mindx ) || ( maxsx != null && tree.val <= maxsx ) )
      {
        return false;
      }

    return checkBST( tree.sx ) && checkBST( tree.dx );
  }



/*** ES7 ***/
// Si scriva una funzione JS minDepth(T), dove T è un albero binario come descritto a lezione (oggetti con chiavi val e sx e dx). La funzione deve restituire la minima profondità dell'albero T: la lunghezza del cammino più corto tra tutti quelli che vanno dalla radice alle sue foglie (la radice ha profondità zero).
function minDepth( tree ) 
  {
    if( tree == null )
      {
        return 0;
      }
    if( tree.sx == null && tree.dx == null )
      {
        return 0;
      }

    return 1 + Math.min( minDepth(tree.sx), minDepth(tree.dx) );
  }



/*** ES8 ***/
// Si scriva una funzione normalizza(T) che, dato un albero k-ario non vuoto T come definito a lezione (oggetti con chiavi val e figli, dove figli è un array di nodi), in cui i nodi hanno valori di tipo stringa, lo modifica sostituendo ad ogni valore s, true se la lunghezza di s è minore o uguale di 3 e false altrimenti.
function normalizza( T )
  {
    if( T == null ) return;
    T.val = (T.val.length <= 3 ) ? true : false;
    if( T.figli == null ) return;
    for( var child of T.figli )
      { 
        normalizza( child );
      }
  }



/*** ES9 ***/
// Si scriva una funzione JavaScript deep(T) che, ricevuto un albero binario (non-vuoto) T rappresentato come visto a lezione, restituisca il valore del campo val del nodo più profondo (cioè, quello più lontano dalla radice). In caso di parità di profondità, la funzione deve restituire il valore del nodo più a sinistra.
function deep( T )
  {
    let dl = -1;
    let val = -1;
    function finder( T, level )
      {
        if( T != null ) 
          {
            finder( T.sx, level+1 );
            finder( T.dx, level+1 );
            if( level > dl )
              {
                val = T.val;
                dl = level;
              }
          }
      }
    finder( T, 0 );
    return val;
  }



/*** E10 ***/
// Si scriva una funzione JS livelloPari(T), dove T è un albero binario come descritto a lezione (oggetti con chiavi val e sx e dx). Si definisce livello di un nodo la sua distanza dalla radice (il livello della radice è 0). Si assuma che i val contenuti nei nodi abbiano come valore numeri interi. Si scriva una funzione che prende in ingresso un albero binario e restituisce true se tutti i nodi di livello pari contengono un numero pari e tutti i nodi di livello dispari contengono un numero dispari, e false altrimenti.
function livelloPari( t )
  {
    let isPari = true;
    function LP( t, livello )
      {
        if( t != null )
          {
            if( (( t.val % 2 == 0 ) && ( livello % 2 != 0 )) || (( t.val % 2 != 0 ) && ( livello % 2 == 0 )) )
              {
                isPari = false;
              }
            LP( t.sx, livello+1 );
            LP( t.dx, livello+1 );
           }
      };
    
    LP( t, 0 );
    return isPari;
  }



/*** ES11 ***/
//Si scriva una funzione sxltdx(T) che, dato un albero binario T come definito a lezione, in cui i nodi hanno valore numerico, modifichi l'albero scambiando fra loro i figli sx e dx di ogni nodo se sono entrambi presenti, in modo che il valore del figlio destro sia sempre maggiore o uguale al valore del figlio sinistro.
function sxltdx( t )
  {
    if( t != null )
      {
        sxltdx( t.sx );
        sxltdx( t.dx );
        if( t.sx != null && t.dx != null )
          {
            if( t.dx.val < t.sx.val )
              {
                let tmp = t.dx;
                t.dx = t.sx;
                t.sx = tmp;
              }
          }
      }
  }


/*** ES12 ***/
// Si scriva una funzione visitaLivelli(t), con t un albero binario. La funzione deve stampare i nodi t per livelli. Quindi, la funzione deve stampare prima tutti i nodi a livello 0 (la radice), poi tutti quelli a livello 1 da sinistra a destra, e così via.
function visitaLivelli( t )
  {
    if( t != null )
      {
        let queue = new Array();
        queue.push( t );
        while( queue.length > 0 )
          {
            let n = queue.shift();
            console.log( n.val );
            if( n.sx != null )
              queue.push( n.sx );
            if( n.dx != null )
              queue.push( n.dx );
          }
      }
  }



/*** ES13 ***/
// Diciamo che un nodo di un albero binario è in equilibrio se il suo valore è >= del valore del suo figlio sinistro (se esiste), ed è <= del valore del suo figlio destro (se esiste). Diciamo che un albero binario è in equilibrio se tutti i suoi nodi sono in equilibrio.
// Si scriva una funzione inEquilibrio(t) che dato un albero binario (i cui nodi sono implementati come visto a lezione come oggetti con chiavi val, sx e dx) restituisca true se t è in equilibrio, e false altrimenti.
function inEquilibrio( t )
  {
    let isin = true;
    function eq( t )
      {
        if( t != null )
          {
            if( t.sx != null && t.sx.val > t.val )
              {
                isin = false;
              }
            if( t.dx != null && t.dx.val < t.val )
              {
                isin = false;
              }
            eq( t.sx );
            eq( t.dx );
          }
      };
    eq( t );
    return isin;
  }

// console.log( inEquilibrio({val:7,sx:{val: 4, sx: {val: 3}, dx: {val:12, sx: {val: 4, dx:{val:8}, sx:{val: 2}}}}, dx:{val: 11, dx: {val: 18}, sx: {val:3, sx: {val: 2}}}}) )
// console.log( inEquilibrio({val:8,sx:{val: -4, sx: {val: 33}, dx: {val:13, sx: {val: 4, dx:{val:-3}, sx:{val: 81}}}}, dx:{val: 11, dx: {val: 3}, sx: {val:8, sx: {val: 63}}}}) );
// console.log( inEquilibrio({val:8}) );
// console.log( inEquilibrio({val:8,sx:{val: 8},dx:{val:8}}) );



/***  ES14 ***/
// Si scriva una funzione verifica(T) che, dato un albero binario non vuoto T come definito a lezione (oggetti con chiavi val e sx e dx), restituisce true se il valore memorizzato in ogni nodo è maggiore stretto di quello memorizzato nei suoi due figli, e false altrimenti.
function verifica( T )
  {
    let isgt = true;
    function check( t )
      {
        if( t != null )
          {
            if( ( t.sx != null && t.val <= t.sx.val ) || ( t.dx != null && t.val <= t.dx.val ) )
              {
                isgt = false;
              }
            check( t.sx );
            check( t.dx );
          }
      }

    check( T );
    return isgt;
  }



